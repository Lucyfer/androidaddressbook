package com.example;

import android.app.Activity;
import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.*;
import java.util.LinkedList;

public class MyActivity extends TabActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        TabHost tabHost = getTabHost();
        //tabHost.setup();

        TabHost.TabSpec addSpec = tabHost.newTabSpec("Add Contacts");
        addSpec.setIndicator("Add Contacts");
        //addSpec.setContent(R.id.addTabLayout);
        addSpec.setContent(new Intent(this, AddActivity.class));

        TabHost.TabSpec searchSpec = tabHost.newTabSpec("Search Contacts");
        searchSpec.setIndicator("Search Contacts");
        //searchSpec.setContent(R.id.searchTabLayout);
        searchSpec.setContent(new Intent(this, SearchActivity.class));

        tabHost.addTab(addSpec);
        tabHost.addTab(searchSpec);
        //tabHost.setCurrentTab(0);
    }
}
