package com.example;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.Console;
import java.nio.DoubleBuffer;
import java.util.LinkedList;

public class ContactsHelper extends SQLiteOpenHelper {

    public static final String Tablename = "contacts";
    public static final String Name = "name";
    public static final String Phone = "phone";

    public ContactsHelper(Context context) {
        super(context, Tablename, null, 1);
    }
    
    public LinkedList<ContactItem> getContacts() {
        SQLiteDatabase db = getReadableDatabase();
        LinkedList<ContactItem> ci = new LinkedList<ContactItem>();
        Cursor cur = db.query(Tablename, new String[] {Name, Phone}, null, null, null, null, null);

        while (cur.moveToNext()) {
            ci.add(new ContactItem(cur.getString(0), cur.getString(1)));
        }
        cur.close();
        db.close();
        return ci;
    }
    
    public LinkedList<ContactItem> searchContacts(String name, String phone) {
        SQLiteDatabase db = getReadableDatabase();
        LinkedList<ContactItem> ci = new LinkedList<ContactItem>();
        Cursor cur = db.rawQuery("select name,phone from " + Tablename + " where name LIKE '" + name + "%' AND phone LIKE '" + phone + "%' ",
                null);
        while (cur.moveToNext()) {
            ci.add(new ContactItem(cur.getString(0), cur.getString(1)));
        }
        cur.close();
        db.close();
        return ci;
    }
    
    public long saveContact(String name, String phone) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(Name, name);
        cv.put(Phone, phone);
        long id = db.insert(Tablename, null, cv);
        db.close();
        return id;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("create table " + Tablename +
                "(_id integer primary key autoincrement, " +
                Name + " text, " +
                Phone + " text" + ");");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("drop table if exists " + Tablename);
        onCreate(sqLiteDatabase);
    }
}
