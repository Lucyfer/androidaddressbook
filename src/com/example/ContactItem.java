package com.example;

import java.util.HashMap;

public class ContactItem extends HashMap<String, String> {

    public static final String Name = "name";
    public static final String Phone = "phone";

    public ContactItem(String name, String phone) {
        super();
        super.put(Name, name);
        super.put(Phone, phone);
    }

    @Override
    public String toString() {
        return this.get(Name) + " : " + this.get(Phone);
    }
}
