package com.example;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import java.util.LinkedList;

public class AddActivity extends Activity {

    protected EditText name;
    protected EditText phone;
    protected ListView phone_list;
    protected ContactsHelper cHelper;
    protected LinkedList<ContactItem> records;
    protected SimpleAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add);

        name = (EditText) findViewById(R.id.name);
        phone = (EditText) findViewById(R.id.phone);
        phone_list = (ListView) findViewById(R.id.phone_list);
        cHelper = new ContactsHelper(getApplicationContext());
        records = cHelper.getContacts();
        adapter = new SimpleAdapter(
                getApplicationContext(),
                records,
                R.layout.list,
                new String[] {ContactItem.Name, ContactItem.Phone},
                new int[] {R.id.name, R.id.phone});
        phone_list.setAdapter(adapter);
    }

    public void addRecord(View v) {
        String n = name.getText().toString().trim();
        String p = phone.getText().toString().trim();
        if (n.isEmpty() || p.isEmpty())
            return;

        cHelper.saveContact(n, p);
        records.add(new ContactItem(n, p));
        name.setText("");
        phone.setText("");
        adapter.notifyDataSetChanged();
        Toast toast = Toast.makeText(getApplicationContext(), "Record added", Toast.LENGTH_SHORT);
        toast.show();
    }
}
