package com.example;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.LinkedList;

public class SearchActivity extends Activity {

    protected EditText name;
    protected EditText phone;
    protected ListView phone_list;
    protected ContactsHelper cHelper;
    protected LinkedList<ContactItem> records;
    protected SimpleAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search);

        name = (EditText) findViewById(R.id.name);
        phone = (EditText) findViewById(R.id.phone);
        phone_list = (ListView) findViewById(R.id.phone_list);
        cHelper = new ContactsHelper(getApplicationContext());
        records = new LinkedList<ContactItem>();
        adapter = new SimpleAdapter(
                getApplicationContext(),
                records,
                R.layout.list,
                new String[] {ContactItem.Name, ContactItem.Phone},
                new int[] {R.id.name, R.id.phone});
        phone_list.setAdapter(adapter);
    }

    public void searchRecord(View v) {
        String n = name.getText().toString().trim();
        String p = phone.getText().toString().trim();

        records.clear();
        records.addAll(cHelper.searchContacts(n, p));
        adapter.notifyDataSetChanged();
    }
}
